@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 my-4">
                <h4><b>Order</b> #{{ $order->id }}
                    @switch($order->status)
                        @case('rejected')
                        <span class="badge badge-danger float-right">{{ $order->status }}</span>
                        @break
                        @case('in progress')
                        <span class="badge badge-warning float-right">{{ $order->status }}</span>
                        @break
                        @case('ready')
                        <span class="badge badge-success float-right">{{ $order->status }}</span>
                        @break
                        @case('closed')
                        <span class="badge badge-default float-right">{{ $order->status }}</span>
                        @break
                        @default
                        <span class="badge badge-secondary float-right">{{ $order->status }}</span>
                    @endswitch
                </h4>
                <h5><b>Store:</b> {{ $order->store->name }}</h5>
                <h5><b>Payment:</b> {{ $order->payment }}</h5>
                <h5><b>Pick up:</b> {{ $order->pickup }}</h5>
                <h5><b>Notes:</b> {{ $order->notes ?? 'N/A' }}</h5>
                <h5><b>Created:</b> {{ $order->created_at->format('D d-M-Y h:i A') }}</h5>

                @if($order->transaction()->exists())
                    <h5><b>Transaction:</b> #{{ $order->transaction->id }}</h5>
                @endif
                <h5><b>Total:</b> <i class="fa fa-pound-sign"></i> {{ $order->total }}</h5>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                @foreach($order->products as $product)
                    <li class="list-group-item d-flex justify-content-between lh-condensed align-items-center">
                        <div class="flex-grow-1">
                            <h6 class="my-0 d-flex align-items-center">
                                <a href="{{ $product->path }}"
                                   class="mr-2">{{ $product->title }}</a>
                                <span class="badge badge-secondary">{{ $product->pivot->quantity }}</span>
                            </h6>
                        </div>
                        <span class="text-muted">
                            <i class="fa fa-pound-sign"></i>
                            {{ $product->price * $product->pivot->quantity }}
                        </span>
                    </li>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-12 d-flex">
                @if($order->status == 'pending' or $order->status == 'in progress')
                    <div class="mt-2 mr-1">
                        <!-- Notes modal trigger-->
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal"
                                data-target="#add-notes">
                            <i class="fa fa-plus"></i> Add notes
                        </button>
                    </div>
                    <!-- Notes modal -->
                    <div class="modal fade" id="add-notes" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Add notes to Order #{{ $order->id }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('orders.update', $order->id) }}" method="POST">
                                    @csrf
                                    @method('PATCH')
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="notes">Notes</label>
                                            <textarea name="notes" id="notes" rows="5" class="form-control"></textarea>
                                        </div>
                                    </div>

                                    <input type="hidden" name="add_notes">
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endif

                @if($order->status == 'pending' or $order->status == 'in progress')
                    <div class="mt-2">
                        <!-- Schedule trigger modal -->
                        <button type="button" class="btn btn-outline-success" data-toggle="modal"
                                data-target="#add-schedule">
                            <i class="fa fa-clock"></i> Schedule
                        </button>
                    </div>
                    <!-- Schedule modal -->
                    <div class="modal fade" id="add-schedule" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Schedule Order #{{ $order->id }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('orders.update', $order->id) }}" method="POST">
                                    @csrf
                                    @method('PATCH')
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="pickup_at">Pick up at</label>
                                            <select name="pickup_at" id="pickup_at" class="form-control">
                                                <option value="asap">ASAP</option>
                                                <option value="30m">30 minutes</option>
                                                <option value="1h">1 hour</option>
                                                <option value="2h">2 hours</option>
                                                <option value="3h">3 hours</option>
                                            </select>
                                        </div>
                                    </div>

                                    <input type="hidden" name="add_schedule">

                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
