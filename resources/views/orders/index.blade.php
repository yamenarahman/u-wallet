@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="alert alert-info" role="alert">
            <strong>Note:</strong>
            <i class="fa fa-eye" aria-hidden="true"></i> View order to add notes or schedule a pick up time.
        </div>
        <div class="row">
            <div class="col-12 my-4">
                <h4><i class="fa fa-server" aria-hidden="true"></i> Orders</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive bg-white shadow-sm p-3">
                    <table class="table table-hover">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Store</th>
                                <th>Status</th>
                                <th>Payment method</th>
                                <th>Total</th>
                                <th>Created</th>
                                <th>Details</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($orders->isEmpty())
                                <tr>
                                    <td colspan="6" class="text-center">No orders yet.</td>
                                </tr>
                            @endif
                            @foreach ($orders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td><a href="{{ $order->store->path }}">{{ $order->store->name }}</a></td>
                                    <td>
                                        @switch($order->status)
                                            @case('rejected')
                                                <span class="badge badge-danger">{{ $order->status }}</span>
                                                @break
                                            @case('in progress')
                                                <span class="badge badge-warning">{{ $order->status }}</span>
                                                @break
                                            @case('ready')
                                                <span class="badge badge-success">{{ $order->status }}</span>
                                                @break
                                            @case('closed')
                                                <span class="badge badge-default">{{ $order->status }}</span>
                                                @break
                                            @default
                                                <span class="badge badge-secondary">{{ $order->status }}</span>
                                        @endswitch
                                    </td>
                                    <td>
                                        <span class="badge badge-pill badge-info">{{ $order->payment }}</span>
                                        @if($order->transaction()->exists())
                                            Transaction #{{ $order->transaction->id }}
                                        @endif
                                    </td>
                                    <td><i class="fas fa-pound-sign"></i> {{ $order->total }}</td>
                                    <td>{{ $order->created_at->format('D d-M-Y h:i A') }}</td>
                                    <td>
                                        <a href="{{ $order->path }}" class="btn btn-default"><i class="fa fa-eye"></i> view</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
