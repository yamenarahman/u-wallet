@extends('layouts.stores.app')

@section('content')
    <div class="container-fluid">
        <h1><i class="fa fa-database"></i> {{ $category->name }}</h1>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive bg-white shadow-lg p-3">
                    <table class="table table-hover datatable">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Price</th>
                                <th>Created</th>
                                <th class="text-center"><i class="fa fa-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($category->products->isNotEmpty())
                                @php $i=1 @endphp
                                @foreach($category->products as $product)
                                    <tr>
                                        <td scope="row">{{ $i }}</td>
                                        <td><img src="{{ $product->imagePath }}" alt="{{ $product->title }}"
                                                 width="100px" height="100px" class="img-fluid img-thumbnail"></td>
                                        <td>{{ $product->title }}</td>
                                        <td><i class="fa fa-pound-sign"></i> {{ $product->price }}</td>
                                        <td>{{ $product->created_at->diffForHumans() }}</td>
                                        <td class="d-flex align-items-center justify-content-center">
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn fa fa-edit text-secondary" data-toggle="modal"
                                                    data-target="#edit-category-{{ $product->id }}">
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="edit-category-{{ $product->id }}" tabindex="-1"
                                                 role="dialog"
                                                 aria-labelledby="modelTitleId" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit category '{{ $product->name }}'</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{ route('owner.categories.update', $product->id) }}"
                                                              method="POST">
                                                            @csrf
                                                            @method('PATCH')
                                                            <div class="modal-body">
                                                                <div class="form-group row">
                                                                    <label for="name"
                                                                           class="control-label col-2 align-self-center">Name</label>
                                                                    <input type="text" class="form-control col-10"
                                                                           name="name" id="name"
                                                                           value="{{ $product->name }}" required>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary">Save</button>
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <form action="{{ route('owner.categories.destroy', $product->id) }}" method="POST"
                                                  id="delete-category-{{ $product->id }}">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn"
                                                        onclick="confirmDelete('delete-category-{{ $product->id }}')">
                                                    <i class="fa fa-trash text-danger"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @php $i++ @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4" class="text-center">No products yet.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function confirmDelete(id) {
            event.preventDefault();
            swal({
                title: 'Are you sure?',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + id).submit();
                }
            });
        }
    </script>
@endpush
