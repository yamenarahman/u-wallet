@extends('layouts.stores.app')

@section('content')
    <div class="container-fluid">
        <h1><i class="fa fa-box-open"></i> Products
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#create-product">
                <i class="fa fa-plus-circle"></i>
            </button>

        </h1>
        <div class="modal fade" id="create-product" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create new product</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @if($categories->isNotEmpty())
                        <form action="{{ route('owner.products.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="title" class="control-label align-self-center">Title</label>
                                    <input type="text" class="form-control" name="title" id="title"
                                           value="{{ old('title') }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="price" class="control-label align-self-center">Price</label>
                                    <input type="number" step="0.01" class="form-control" name="price" id="price"
                                           value="{{ old('price') }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="description"
                                           class="control-label align-self-center">Description</label>
                                    <textarea class="form-control" name="description"
                                              id="description">{{ old('descripiton') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="category_id" class="control-label align-self-center">Category</label>
                                    <select class="form-control" name="category_id" id="category_id">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <input type="file" class="form-control-file" name="image" id="image">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    @else
                        <div class="modal-body d-flex justify-content-center">
                            <a href="/owner/categories" class="btn btn-warning"><i
                                    class="fa fa-exclamation-triangle"></i> create category first</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="table-responsive bg-white shadow-lg p-3">
                    <table class="table table-hover datatable">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Price</th>
                                <th>Category</th>
                                <th>Created</th>
                                <th class="text-center"><i class="fa fa-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($products->isNotEmpty())
                                @php $i=1 @endphp
                                @foreach($products as $product)
                                    <tr>
                                        <td scope="row">{{ $i }}</td>
                                        <td><img src="{{ $product->imagePath }}" alt="{{ $product->title }}"
                                                 width="100px" height="100px" class="img-fluid img-thumbnail"></td>
                                        <td>{{ $product->title }}</td>
                                        <td><i class="fa fa-pound-sign"></i> {{ $product->price }}</td>
                                        <td>{{ $product->category->name }}</td>
                                        <td>{{ $product->created_at->diffForHumans() }}</td>
                                        <td class="d-flex align-items-center justify-content-center">
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn fa fa-edit text-secondary"
                                                    data-toggle="modal"
                                                    data-target="#edit-product-{{ $product->id }}">
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="edit-product-{{ $product->id }}" tabindex="-1"
                                                 role="dialog"
                                                 aria-labelledby="modelTitleId" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">
                                                                Edit product '{{ $product->title }}'
                                                            </h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form
                                                            action="{{ route('owner.products.update', $product->id) }}"
                                                            method="POST"
                                                            enctype="multipart/form-data"
                                                        >
                                                            @csrf
                                                            @method('PATCH')
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <label for="title"
                                                                           class="control-label align-self-center">Title</label>
                                                                    <input type="text" class="form-control" name="title"
                                                                           id="title"
                                                                           value="{{ $product->title }}" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="price"
                                                                           class="control-label align-self-center">Price</label>
                                                                    <input type="number" step="0.01"
                                                                           class="form-control" name="price" id="price"
                                                                           value="{{ $product->price }}" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="description"
                                                                           class="control-label align-self-center">Description</label>
                                                                    <textarea class="form-control" name="description"
                                                                              id="description">{{ $product->description }}</textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="category_id"
                                                                           class="control-label align-self-center">Category</label>
                                                                    <select class="form-control" name="category_id"
                                                                            id="category_id">
                                                                        @foreach($categories as $category)
                                                                            <option
                                                                                value="{{ $category->id }}"
                                                                                @if($category->id == $product->category_id) selected @endif>
                                                                                {{ $category->name }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="image">Image</label>
                                                                    <input type="file" class="form-control-file"
                                                                           name="image" id="image">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary">Save
                                                                </button>
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <form action="{{ route('owner.products.destroy', $product->id) }}"
                                                  method="POST"
                                                  id="delete-product-{{ $product->id }}">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn"
                                                        onclick="confirmDelete('delete-product-{{ $product->id }}')">
                                                    <i class="fa fa-trash text-danger"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @php $i++ @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7" class="text-center">No products yet.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function confirmDelete(id) {
            event.preventDefault();
            swal({
                title: 'Are you sure?',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + id).submit();
                }
            });
        }
    </script>
@endpush
