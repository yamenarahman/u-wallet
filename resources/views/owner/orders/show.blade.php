@extends('layouts.stores.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 my-4">
                <h4><b>Order</b> #{{ $order->id }}
                    @switch($order->status)
                        @case('rejected')
                        <span class="badge badge-danger float-right">{{ $order->status }}</span>
                        @break
                        @case('in progress')
                        <span class="badge badge-warning float-right">{{ $order->status }}</span>
                        @break
                        @case('ready')
                        <span class="badge badge-success float-right">{{ $order->status }}</span>
                        @break
                        @case('closed')
                        <span class="badge badge-default float-right">{{ $order->status }}</span>
                        @break
                        @default
                        <span class="badge badge-secondary float-right">{{ $order->status }}</span>
                    @endswitch
                </h4>
                <h5><b>Customer:</b> {{ $order->customer->name }}</h5>
                <h5><b>Customer phone number:</b> {{ $order->customer->profile->phone }}</h5>
                <h5><b>Payment:</b> {{ $order->payment }}</h5>
                <h5><b>Pick up:</b> {{ $order->pickup }}</h5>
                <h5><b>Notes:</b> {{ $order->notes ?? 'N/A' }}</h5>
                <h5><b>Created:</b> {{ $order->created_at->format('D d-M-Y h:i A') }}</h5>

            @if($order->transaction()->exists())
                    <h5><b>Transaction:</b> #{{ $order->transaction->id }}</h5>
                @endif
                <h5><b>Total:</b> <i class="fa fa-pound-sign"></i> {{ $order->total }}</h5>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                @foreach($order->products as $product)
                    <li class="list-group-item d-flex justify-content-between lh-condensed align-items-center">
                        <div class="flex-grow-1">
                            <h6 class="my-0 d-flex align-items-center">
                                <span class="mr-2">{{ $product->title }}</span>
                                <span
                                    class="badge badge-secondary">{{ $product->pivot->quantity }}</span>
                            </h6>
                        </div>
                        <span class="text-muted">
                            <i class="fa fa-pound-sign"></i>
                            {{ $product->price * $product->pivot->quantity }}
                        </span>
                    </li>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-8 mx-auto mt-5">
                <form action="{{ route('owner.orders.update', $order->id) }}"
                      method="POST" id="{{ 'update-order-' . $order->id }}">
                    @csrf
                    @method('PATCH')

                    <input type="hidden" id="order-status-{{ $order->id }}" name="status"
                           value="{{ $order->status }}">
                </form>

                @if($order->status == 'pending')
                    <button class="btn btn-block btn-outline-primary"
                            onclick="updateOrder('in progress', '{{ $order->id }}')">
                        <i class="fa fa-check-circle"></i> Confirm
                    </button>
                    <button class="btn btn-block btn-outline-danger"
                            onclick="updateOrder('rejected', '{{ $order->id }}')">
                        <i class="fa fa-times"></i> Reject
                    </button>
                @elseif($order->status == 'in progress')
                    <button class="btn btn-block btn-outline-success"
                            onclick="updateOrder('ready', '{{ $order->id }}')">
                        <i class="fa fa-shopping-bag"></i> Ready for pick up
                    </button>
                @elseif($order->status == 'ready')
                    <button class="btn btn-block btn-outline-dark"
                            onclick="updateOrder('closed', '{{ $order->id }}')">
                        <i class="fa fa-arrow-circle-down"></i> Close
                    </button>
                @endif
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function updateOrder(status, orderId) {
            $('#order-status-' + orderId).val(status);
            $('#update-order-' + orderId).submit();
        }
    </script>
@endpush
