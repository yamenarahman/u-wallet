@extends('layouts.stores.app')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><i class="fa fa-tachometer-alt"></i> Dashboard - {{ auth()->user()->store->name }}</h1>

        <div class="card-deck">
            <div class="card shadow-sm">
                <div class="card-body">
                    <h4 class="card-title text-center text-primary">
                        <a href="/owner/categories" class="prevent-default">Categories</a>
                    </h4>
                    <p class="card-text text-center text-secondary">{{ $stats['categories'] }}</p>
                </div>
            </div>
            <div class="card shadow-sm">
                <div class="card-body">
                    <h4 class="card-title text-center text-primary">
                        <a href="/owner/products" class="prevent-default">Products</a>
                    </h4>
                    <p class="card-text text-center text-secondary">{{ $stats['products'] }}</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
