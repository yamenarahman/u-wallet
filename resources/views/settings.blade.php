@extends('layouts.app')

@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-cogs"></i> Settings</h1>

        <div class="card-deck mb-5">
            <div class="card shadow-sm">
                <div class="card-body">
                    <form action="{{ route('settings.save') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <legend>Update profile</legend>
                        <div class="form-group">
                            <label for="avatar">Avatar</label>
                            <input type="file" name="avatar" id="avatar" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="phone">Phone number</label>
                            <input type="text" name="phone" id="phone" class="form-control" value="{{ auth()->user()->profile->phone ?? '' }}" required>
                        </div>

                        <div class="form-group">
                            <label for="faculty">Faculty</label>
                            <input type="text" name="faculty" id="faculty" class="form-control" value="{{ auth()->user()->profile->faculty ?? '' }}">
                        </div>

                        <input type="hidden" name="update_profile">
                        <button class="btn btn-primary float-right my-3" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="card-deck">
            <div class="card shadow-sm">
                <div class="card-body">
                    <form action="{{ route('settings.save') }}" method="post">
                        @csrf

                        <legend>Update password</legend>
                        <div class="form-group">
                            <label for="current_password">Current password</label>
                            <input type="password" name="current_password" id="current_password" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="password">New password</label>
                            <input type="password" name="password" id="password" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="password_confirmation">Confirm new password</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" required>
                        </div>

                        <input type="hidden" name="update_password">
                        <button class="btn btn-primary float-right my-3" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
