<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">
    @laravelPWA
</head>
<body>
    <div class="se-pre-con"></div>
    <div id="app" class="full-height d-flex flex-column">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <i class="fa fa-credit-card"></i> {{ config('app.name') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @auth
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/cart') }}">
                                    <i class="fa fa-cart-arrow-down" aria-hidden="true"></i> Cart
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/orders') }}">
                                    <i class="fa fa-server" aria-hidden="true"></i> Orders
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/wallet') }}"><i class="fas fa-wallet"></i> My wallet</a>
                            </li>
                            <li class="nav-item ml-2">
                                <form action="{{ route('search') }}" class="d-flex" method="GET">
                                    <input type="text" class="form-control mx-1" name="query">
                                    <button class="btn btn-outline-primary btn-sm" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </form>
                            </li>
                        @endauth
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <notifications user-id="{{ auth()->id() }}"></notifications>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fa fa-user" aria-hidden="true"></i> {{ Auth::user()->name }} <span
                                        class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="/settings">
                                        <i class="fas fa-cogs"></i>
                                        Settings
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out-alt" aria-hidden="true"></i> {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4 flex-grow-1">
            @yield('content')
        </main>
        <!-- Footer -->
        <footer class="py-5 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; U-wallet {{ now()->year }}</p>
            </div>
            <!-- /.container -->
        </footer>
    </div>
    <!-- Scripts -->
    <script src="{{ asset(mix('js/app.js')) }}"></script>
    @stack('js')
    @if(session('message'))
        <script>
            toastr.{{ session('type') }}("{{ session('message') }}");
        </script>
    @endif

    @if($errors->any())
        <script>
            @foreach($errors->all() as $error)
            toastr.error("{{ $error }}");
            @endforeach
        </script>
    @endif

    <script>
        $(document).ready(() => {
            $('.datatable').DataTable({
                sort: false
            });
        });
    </script>

    <script>
        // Wait for window load
        $(window).on('load', () => {
            // Animate loader off screen
            $(".se-pre-con").fadeOut("slow");
        });
    </script>
</body>
</html>
