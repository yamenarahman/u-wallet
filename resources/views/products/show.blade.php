@extends('layouts.app')

@section('content')
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-lg-3">
                <h1 class="my-4">{{ $product->category->store->name }}</h1>
                <div class="list-group">
                    @foreach($product->category->store->categories as $category)
                        <a href="{{ $category->path }}" class="list-group-item">{{ $category->name }}</a>
                    @endforeach
                </div>
            </div>
            <!-- /.col-lg-3 -->

            <div class="col-lg-9">

                <div class="card mt-4">
                    <img class="card-img-top img-fluid" src="{{ $product->image ? $product->imagePath : 'https://placehold.it/900x400' }}" alt="">
                    <div class="card-body">
                        <h3 class="card-title">{{ $product->name }}</h3>
                        <h4><i class="fa fa-pound-sign"></i> {{ $product->price }}</h4>
                        <p class="card-text">{{ $product->description }}</p>
                        <div class="d-flex justify-content-between">
                            <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9733;</span>
                            <form action="{{ route('cart.update') }}" method="POST">
                                @csrf
                                @method('PATCH')
                                <input type="hidden" name="add_item">
                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                                <button class="btn btn-outline-success btn-sm" type="submit">
                                    Add to cart
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.card -->

            </div>
            <!-- /.col-lg-9 -->

        </div>

    </div>
    <!-- /.container -->
@endsection
