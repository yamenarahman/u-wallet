@extends('layouts.app')

@section('content')
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-lg-3">

                <h1 class="my-4"><a href="{{ $category->store->path }}"><i
                            class="fa fa-chevron-left fa-sm"></i></a> {{ $category->store->name }}</h1>
                <div class="list-group">
                    @foreach($category->store->categories as $storeCategory)
                        <a href="{{ $storeCategory->path }}"
                           class="list-group-item {{ $storeCategory->id == $category->id ? 'active' : '' }}">{{ $storeCategory->name }}</a>
                    @endforeach
                </div>
            </div>
            <!-- /.col-lg-3 -->

            <div class="col-lg-9">
                <div class="row">
                    @foreach($category->products as $product)
                        <div class="col-lg-4 col-md-6 mb-4">
                            <div class="card h-100">
                                <a href="{{ $product->path }}"><img class="card-img-top"
                                                                    src="{{ $product->image ? $product->imagePath : 'https://placehold.it/700x400' }}"
                                                                    alt=""></a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <a href="{{ $product->path }}">{{ $product->title }}</a>
                                    </h4>
                                    <h5><i class="fa fa-pound-sign"></i> {{ $product->price }}</h5>
                                    <p class="card-text">{{ \Illuminate\Support\Str::words($product->description, 20) }}</p>
                                </div>
                                <div class="card-footer d-flex justify-content-between">
                                    <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9733;</span>
                                    <form action="{{ route('cart.update') }}" method="POST">
                                        @csrf
                                        @method('PATCH')
                                        <input type="hidden" name="add_item">
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        <button class="btn btn-outline-success btn-sm" type="submit">
                                            Add to cart
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- /.row -->

            </div>
            <!-- /.col-lg-9 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
@endsection
