@extends('layouts.admin.app')

@section('content')
    <div class="container-fluid">
        <h1><i class="fa fa-home"></i> Stores
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#create-store">
                <i class="fa fa-plus-circle"></i>
            </button>

        </h1>
        <div class="modal fade" id="create-store" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create new store</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('admin.stores.store') }}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="name" class="control-label col-2 align-self-center">Name</label>
                                <input type="text" class="form-control col-10" name="name" id="name"
                                       value="{{ old('name') }}" required>
                            </div>
                            <div class="form-group row">
                                <label for="owner" class="control-label col-2 align-self-center">Store owner</label>
                                <select name="owner_id" id="owner" class="form-control col-10">
                                    @foreach($owners as $owner)
                                        <option value="{{ $owner->id }}">{{ $owner->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="table-responsive bg-white shadow-lg p-3">
                    <table class="table table-hover datatable">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Store</th>
                                <th>Owner</th>
                                <th>Created</th>
                                <th class="text-center"><i class="fa fa-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($stores->isNotEmpty())
                            @php $i=1 @endphp
                            @foreach($stores as $store)
                                <tr>
                                    <td scope="row">{{ $i }}</td>
                                    <td>{{ $store->name }}</td>
                                    <td>{{ $store->owner->name }}</td>
                                    <td>{{ $store->created_at->diffForHumans() }}</td>
                                    <td class="d-flex align-items-center justify-content-center">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn fa fa-edit text-secondary" data-toggle="modal"
                                                data-target="#edit-store-{{ $store->id }}">
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="edit-store-{{ $store->id }}" tabindex="-1"
                                             role="dialog"
                                             aria-labelledby="modelTitleId" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Edit store '{{ $store->name }}'</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{ route('admin.stores.update', $store->id) }}"
                                                          method="POST">
                                                        @csrf
                                                        @method('PATCH')
                                                        <div class="modal-body">
                                                            <div class="form-group row">
                                                                <label for="name"
                                                                       class="control-label col-2 align-self-center">Name</label>
                                                                <input type="text" class="form-control col-10"
                                                                       name="name" id="name"
                                                                       value="{{ $store->name }}" required>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="owner"
                                                                       class="control-label col-2 align-self-center">Store
                                                                    owner</label>
                                                                <select name="owner_id" id="owner"
                                                                        class="form-control col-10">
                                                                    @foreach($owners as $owner)
                                                                        <option
                                                                            value="{{ $owner->id }}" {{ $owner->id == $store->owner_id ? 'selected' : ''}}>
                                                                            {{ $owner->name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <form action="{{ route('admin.stores.destroy', $store->id) }}" method="POST"
                                              id="delete-store-{{ $store->id }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn"
                                                    onclick="confirmDelete('delete-store-{{ $store->id }}')">
                                                <i class="fa fa-trash text-danger"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @php $i++ @endphp
                            @endforeach
                                @else
                                <tr>
                                    <td colspan="5" class="text-center">No stores yet.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function confirmDelete(id) {
            event.preventDefault();
            swal({
                title: 'Are you sure?',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + id).submit();
                }
            });
        }
    </script>
@endpush
