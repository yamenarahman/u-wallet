@extends('layouts.admin.app')

@section('content')
    <div class="container-fluid">
        <h1><i class="fa fa-exchange-alt"></i> Transactions
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                    data-target="#create-transaction">
                <i class="fa fa-plus-circle"></i>
            </button>

        </h1>
        <div class="modal fade" id="create-transaction" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create new transaction</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('admin.transactions.store') }}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="customer" class="control-label col-2 align-self-center">Customer</label>
                                <select name="customer_id" id="customer" class="form-control col-10">
                                    @foreach($customers as $customer)
                                        <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="control-label col-2 align-self-center">Amount</label>
                                <input type="number" class="form-control col-10" name="amount" id="amount"
                                       value="{{ old('amount') }}" step="0.01" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="table-responsive bg-white shadow-lg p-3">
                    <table class="table table-hover datatable">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>User</th>
                                <th>Transaction amount</th>
                                <th>Type</th>
                                <th>Created</th>
                                <th class="text-center"><i class="fa fa-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($transactions->isNotEmpty())
                                @php $i=1 @endphp
                                @foreach($transactions as $transaction)
                                    <tr>
                                        <td scope="row">{{ $i }}</td>
                                        <td>{{ $transaction->customer->name ?? '11' }}</td>
                                        <td><i class="fa fa-pound-sign"
                                               aria-hidden="true"></i> {{ $transaction->amount }}</td>
                                        <td>
                                            <span class="badge badge-primary">{{ $transaction->type }}</span>
                                            @if($transaction->order)
                                                Order #{{ $transaction->order_id }}
                                            @endif
                                        </td>
                                        <td>{{ $transaction->created_at->diffForHumans() }} by
                                            <strong>{{ $transaction->creator->name }}</strong>
                                        </td>
                                        <td class="d-flex align-items-center justify-content-center">
                                            @can('delete', $transaction)
                                                <form
                                                    action="{{ route('admin.transactions.destroy', $transaction->id) }}"
                                                    method="POST"
                                                    id="delete-transaction-{{ $transaction->id }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn"
                                                            onclick="confirmDelete('delete-transaction-{{ $transaction->id }}')">
                                                        <i class="fa fa-trash text-danger"></i></button>
                                                </form>
                                            @endcan
                                        </td>
                                    </tr>
                                    @php $i++ @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center">No transactions yet.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function confirmDelete(id) {
            event.preventDefault();
            swal({
                title: 'Are you sure?',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + id).submit();
                }
            });
        }
    </script>
@endpush
