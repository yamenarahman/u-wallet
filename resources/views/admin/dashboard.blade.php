@extends('layouts.admin.app')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><i class="fa fa-tachometer-alt"></i> Dashboard</h1>

        <div class="card-deck">
            <div class="card shadow-sm">
                <div class="card-body">
                    <h4 class="card-title text-center text-primary">
                        <a href="/admin/users" class="prevent-default">Users</a>
                    </h4>
                    <p class="card-text text-center text-secondary">{{ $stats['users'] }}</p>
                </div>
            </div>
            <div class="card shadow-sm">
                <div class="card-body">
                    <h4 class="card-title text-center text-primary">
                        <a href="/admin/stores" class="prevent-default">Stores</a>
                    </h4>
                    <p class="card-text text-center text-secondary">{{ $stats['stores'] }}</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
