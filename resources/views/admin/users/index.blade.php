@extends('layouts.admin.app')

@section('content')
    <div class="container-fluid">
        <h1><i class="fa fa-users"></i> Users
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#create-user">
                <i class="fa fa-plus-circle"></i>
            </button>

        </h1>
        <div class="modal fade" id="create-user" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create new user</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('admin.users.store') }}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="name" class="control-label col-2 align-self-center">Name</label>
                                <input type="text" class="form-control col-10" name="name" id="name"
                                       value="{{ old('name') }}" required>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="control-label col-2 align-self-center">Email</label>
                                <input type="email" class="form-control col-10" name="email" id="email"
                                       value="{{ old('email') }}" required>
                            </div>
                            <div class="form-group row">
                                <label for="role" class="control-label col-2 align-self-center">Role</label>
                                <select name="role" id="role" class="form-control col-10">
                                    <option value="admin">Admin</option>
                                    <option value="store owner">Store owner</option>
                                    <option value="customer">Customer</option>
                                </select>
                            </div>
                            <div class="alert alert-warning" role="alert">
                                <strong><i class="fa fa-exclamation-circle"></i></strong> Default password: 123456
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="table-responsive bg-white shadow-lg p-3">
                    <table class="table table-hover datatable">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Joined</th>
                                <th>Role</th>
                                <th class="text-center"><i class="fa fa-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i=1 @endphp
                            @foreach($users as $user)
                                <tr>
                                    <td scope="row">{{ $i }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->created_at->diffForHumans() }}</td>
                                    <td>{{ $user->role }}</td>
                                    <td class="d-flex align-items-center justify-content-center">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn fa fa-edit text-secondary" data-toggle="modal"
                                                data-target="#edit-user-{{ $user->id }}">
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="edit-user-{{ $user->id }}" tabindex="-1"
                                             role="dialog"
                                             aria-labelledby="modelTitleId" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Edit user '{{ $user->name }}'</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{ route('admin.users.update', $user->id) }}"
                                                          method="POST">
                                                        @csrf
                                                        @method('PATCH')
                                                        <div class="modal-body">
                                                            <div class="form-group row">
                                                                <label for="name"
                                                                       class="control-label col-2 align-self-center">Name</label>
                                                                <input type="text" class="form-control col-10"
                                                                       name="name" id="name" value="{{ $user->name }}"
                                                                       required>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="email"
                                                                       class="control-label col-2 align-self-center">Email</label>
                                                                <input type="email" class="form-control col-10"
                                                                       name="email" id="email"
                                                                       value="{{ $user->email }}" required>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="role"
                                                                       class="control-label col-2 align-self-center">Role</label>
                                                                <select name="role" id="role"
                                                                        class="form-control col-10">
                                                                    @foreach($roles as $role)
                                                                        <option
                                                                            value="{{ $role }}" {{ $role == $user->role ? 'selected' : '' }}>
                                                                            {{ $role }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <form action="{{ route('admin.users.destroy', $user->id) }}" method="POST"
                                              id="delete-user-{{ $user->id }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn" onclick="confirmDelete('delete-user-{{ $user->id }}')">
                                                <i class="fa fa-trash text-danger"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @php $i++ @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function confirmDelete(id) {
            event.preventDefault();
            swal({
                title: 'Are you sure?',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + id).submit();
                }
            });
        }
    </script>
@endpush
