@extends('layouts.admin.app')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><i class="fas fa-cogs"></i> Settings</h1>

        <div class="card-deck">
            <div class="card shadow-sm">
                <div class="card-body">
                    <form action="{{ route('admin.settings.save') }}" method="post">
                        @csrf

                        <legend>Update password</legend>
                        <div class="form-group">
                            <label for="current_password">Current password</label>
                            <input type="password" name="current_password" id="current_password" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="password">New password</label>
                            <input type="password" name="password" id="password" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="password_confirmation">Confirm new password</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" required>
                        </div>

                        <button class="btn btn-primary float-right my-3" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
