@extends('layouts.admin.app')

@section('content')
    <div class="container-fluid">
        <h1><i class="fa fa-server" aria-hidden="true"></i> Orders</h1>

        <div class="row">
            <div class="col-12">
                <div class="table-responsive bg-white shadow-lg p-3">
                    <table class="table table-hover datatable">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Store</th>
                                <th>Customer</th>
                                <th>Status</th>
                                <th>Payment</th>
                                <th>Total</th>
                                <th>Details</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($orders->isEmpty())
                                <tr>
                                    <td colspan="5" class="text-center">No orders yet.</td>
                                </tr>
                            @endif
                            @foreach ($orders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->store->name }}</td>
                                    <td>{{ $order->customer->name ?? 'User deleted' }}</td>
                                    <td>
                                        @switch($order->status)
                                            @case('rejected')
                                                <span class="badge badge-danger">{{ $order->status }}</span>
                                                @break
                                            @case('in progress')
                                                <span class="badge badge-warning">{{ $order->status }}</span>
                                                @break
                                            @case('ready')
                                                <span class="badge badge-success">{{ $order->status }}</span>
                                                @break
                                            @case('closed')
                                                <span class="badge badge-default">{{ $order->status }}</span>
                                                @break
                                            @default
                                                <span class="badge badge-secondary">{{ $order->status }}</span>
                                        @endswitch
                                    </td>
                                    <td><span class="badge badge-pill badge-info">{{ $order->payment }}</span></td>
                                    <td><i class="fas fa-pound-sign"></i> {{ $order->total }}</td>
                                    <td>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-outline-dark" data-toggle="modal"
                                                data-target="#{{ 'order-' . $order->id }}">
                                            <i class="fa fa-list" aria-hidden="true"></i> Details
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="{{ 'order-' . $order->id }}" tabindex="-1"
                                             role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Order #{{ $order->id }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @foreach($order->products as $product)
                                                            <li class="list-group-item d-flex justify-content-between lh-condensed align-items-center">
                                                                <div class="flex-grow-1">
                                                                    <h6 class="my-0 d-flex align-items-center">
                                                                        <span class="mr-2">{{ $product->title }}</span>
                                                                        <span
                                                                            class="badge badge-secondary">{{ $product->pivot->quantity }}</span>
                                                                    </h6>
                                                                </div>
                                                                <span class="text-muted">
                                                                    <i class="fa fa-pound-sign"></i>
                                                        {{ $product->price * $product->pivot->quantity }}
                                                    </span>
                                                            </li>
                                                        @endforeach
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
