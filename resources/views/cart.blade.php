@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12 mx-auto">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted"><i class="fa fa-cart-arrow-down"></i> Your cart</span>
                    <span class="badge badge-secondary badge-pill">{{ $cart->totalQuantity }}</span>
                </h4>
                <ul class="list-group mb-3">
                    @foreach($cart->products as $product)
                        <li class="list-group-item d-flex justify-content-between lh-condensed align-items-center">
                            <div class="flex-grow-1">
                                <h6 class="my-0 d-flex align-items-center">
                                    <a href="{{ $product->path }}" class="mr-2">{{ $product->title }}</a>
                                    <span class="badge badge-secondary">{{ $product->pivot->quantity }}</span>
                                </h6>
                                <small>{{ $product->category->store->name}}</small>
                            </div>
                            <span class="text-muted"><i class="fa fa-pound-sign"></i>
                        {{ $product->price * $product->pivot->quantity }}
                    </span>
                            <form action="{{ route('cart.update') }}" method="POST" class="mx-2">
                                @csrf
                                @method('PATCH')
                                <input type="hidden" name="remove_item">
                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                                <button class="btn btn-outline-danger" type="submit"><i class="fa fa-trash"
                                                                                        aria-hidden="true"></i></button>
                            </form>
                        </li>
                    @endforeach
                    <li class="list-group-item d-flex justify-content-between">
                        <span><strong>Total</strong></span>
                        <strong><i class="fa fa-pound-sign"></i> {{ $cart->total }}</strong>
                    </li>
                </ul>
                @if($cart->products->isNotEmpty())
                    <button class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#cart-checkout">
                        Continue to checkout
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="cart-checkout" tabindex="-1" role="dialog"
                         aria-labelledby="modelTitleId" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Checkout</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body d-flex justify-content-center">
                                    <form action="{{ route('orders.store') }}" method="post" id="checkout-form">
                                        @csrf
                                        <input type="hidden" name="payment" id="payment-option">
                                    </form>

                                    <button class="btn btn-outline-primary btn-lg mr-1" onclick="checkout('cash')">
                                        <i class="fas fa-money-check" aria-hidden="true"></i> Pay cash
                                    </button>

                                    @if (auth()->user()->balance >= $cart->total)
                                        <button class="btn btn-outline-primary btn-lg" onclick="checkout('wallet')">
                                            <i class="fas fa-wallet"></i> Wallet (current
                                            balance: <i class="fa fa-pound-sign"></i> {{ auth()->user()->balance }})
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route('cart.empty') }}" method="POST" id="empty-cart"
                          class="my-3 d-flex justify-content-center">
                        @csrf
                        @method('DELETE')
                        <button class="btn"
                                onclick="confirmDelete('empty-cart')">
                            <i class="fa fa-trash text-danger"></i> Empty cart
                        </button>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function confirmDelete(id) {
            event.preventDefault();
            swal({
                title: 'Are you sure?',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#' + id).submit();
                }
            });
        }

        function checkout(option) {
            switch (option) {
                case 'cash':
                    $('#payment-option').val('cash');
                    break;
                case 'wallet':
                    $('#payment-option').val('wallet');
                    break;
            }

            $('#checkout-form').submit();
        }
    </script>
@endpush
