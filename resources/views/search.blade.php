@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted"><i class="fa fa-search"></i>
                        Search results for: '{{ request('query') }}'
                    </span>
                </h4>
                <h6>Stores</h6>
                <ul class="list-group mb-3">
                    @foreach($stores as $store)
                    <li class="list-group-item"><a href="{{ $store->path }}">{{ $store->name }}</a></li>
                    @endforeach
                </ul>
                <h6>Categories</h6>
                <ul class="list-group mb-3">
                    @foreach($categories as $category)
                    <li class="list-group-item"><a href="{{ $category->path }}">{{ $category->name }}</a></li>
                    @endforeach
                </ul>
                <h6>Products</h6>
                <ul class="list-group mb-3">
                    @foreach($products as $product)
                    <li class="list-group-item"><a href="{{ $product->path }}">{{ $product->title }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection
