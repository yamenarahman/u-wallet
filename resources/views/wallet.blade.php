@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12 mx-auto my-4">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span><i class="fa fa-wallet"></i> My wallet balance</span>
                    <span><i class="fas fa-pound-sign"></i> {{ auth()->user()->balance }}</span>
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive bg-white shadow-lg">
                    <table class="table table-hover">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Transaction amount</th>
                                <th>Type</th>
                                <th>Created</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($transactions->isNotEmpty())
                                @php $i=1 @endphp
                                @foreach($transactions as $transaction)
                                    <tr>
                                        <td scope="row">{{ $i }}</td>
                                        <td><i class="fa fa-pound-sign"
                                               aria-hidden="true"></i> {{ $transaction->amount }}</td>
                                        <td>
                                            <span class="badge badge-primary">{{ $transaction->type }}</span>
                                            @if($transaction->order)
                                                <a href="{{ $transaction->order->path }}">Order #{{ $transaction->order_id }}</a>
                                            @endif
                                        </td>
                                        <td>{{ $transaction->created_at->diffForHumans() }} by
                                            <strong>{{ $transaction->creator->name }}</strong></td>
                                    </tr>
                                    @php $i++ @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center">No transactions yet.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
