<?php

Route::get('/', function () {
    if (Auth::check()) {
        return redirect('/home');
    }

    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/notifications', 'NotificationController@index');
Route::patch('/notifications/{id}', 'NotificationController@update');
Route::delete('/notifications', 'NotificationController@destroy');

// admin routes
Route::middleware(['auth', 'role:admin'])->prefix('admin')->name('admin.')->group(function () {
    Route::get('/dashboard', 'Admin\DashboardController@index');
    Route::resource('users', 'Admin\UserController')->except(['create', 'show', 'edit']);
    Route::resource('stores', 'Admin\StoreController')->except(['create', 'show', 'edit']);
    Route::resource('transactions', 'Admin\TransactionController')->only(['index', 'store', 'destroy']);
    Route::get('/orders', 'Admin\OrderController@index');
    Route::get('/settings', 'Admin\SettingsController@index');
    Route::post('/settings', 'Admin\SettingsController@store')->name('settings.save');
});

// store owner routes
Route::middleware(['auth', 'role:store owner', 'has_store'])
    ->prefix('owner')
    ->name('owner.')
    ->group(function () {
        Route::get('/dashboard', 'Owner\DashboardController@index');
        Route::resource('categories', 'Owner\CategoryController')->except(['create', 'edit']);
        Route::resource('products', 'Owner\ProductController')->except(['create', 'edit']);
        Route::resource('orders', 'Owner\OrderController')->only(['index', 'show', 'update']);
        Route::get('/settings', 'Owner\SettingsController@index');
        Route::post('/settings', 'Owner\SettingsController@store')->name('settings.save');
    });

// customer routes
Route::middleware(['auth', 'role:customer', 'has_profile'])->group(function () {
    Route::get('stores/{store}', 'StoreController@show');
    Route::get('products/{product}', 'ProductController@show');
    Route::get('categories/{category}', 'CategoryController@show');
    Route::get('wallet', 'WalletController@index')->name('wallet');
    Route::get('cart', 'CartController@show')->name('cart');
    Route::patch('cart', 'CartController@update')->name('cart.update');
    Route::delete('cart', 'CartController@destroy')->name('cart.empty');
    Route::resource('orders', 'OrderController')->except(['create', 'edit', 'destroy']);
    Route::get('/search', 'SearchController@index')->name('search');
});

Route::get('/settings', 'SettingsController@index');
Route::post('/settings', 'SettingsController@store')->name('settings.save');
