<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    protected $withCount = ['products'];
    protected $appends = ['total', 'path', 'pickup'];
    protected $dates = ['pickup_at'];

    /**
     * Belongs to a store.
     */
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    /**
     * Belongs to a customer.
     */
    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    /**
     * Has many products.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot([
            'price',
            'quantity',
            'subtotal'
        ]);
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class);
    }

    /**
     * Calculate total price.
     */
    public function getTotalAttribute()
    {
        return $this->products_count > 0
            ? $this->products->sum('pivot.subtotal')
            : 0;
    }

    public function getPathAttribute()
    {
        if (auth()->user()->hasRole('store owner')) {
            return url("/owner/orders/{$this->id}");
        }

        return url("/orders/{$this->id}");
    }

    public function getPickupAttribute()
    {
        return is_null($this->attributes['pickup_at'])
            ? 'ASAP'
            : $this->pickup_at->format('h:i a');
    }

    public function chargeCustomer()
    {
        Transaction::makePayment($this->id);
    }
}
