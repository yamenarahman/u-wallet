<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $appends = ['total', 'totalQuantity'];

    /**
     * Belongs to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Belongs to many products.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot(['quantity']);
    }

    /**
     * Total cart's products price.
     */
    public function getTotalAttribute()
    {
        return $this->products->sum(function ($product) {
            return $product->price * $product->pivot->quantity;
        });
    }

    /**
     * Total cart's products quantity.
     */
    public function getTotalQuantityAttribute()
    {
        return $this->products->sum('pivot.quantity');
    }

    public function empty()
    {
        $this->products()->detach();
    }

    public function checkout($payment)
    {
        // if products from multiple stores
        $orders = collect([]);

        $this->products->each(function ($product) use ($orders, $payment) {
            // if orders empty create order
            if ($orders->isEmpty()) {
                $order = $this->user->placeOrder($product, $payment);
                // add order to orders collection
                $orders->add($order);
            } else {
                // if there is an existing order from same product's store
                $order = $orders->where('store_id', $product->category->store_id)->first();
                if ($order) {
                    // attach product to order
                    $order->products()->attach($product, [
                        'price' => $product->price,
                        'quantity' => $product->pivot->quantity,
                        'subtotal' => ($product->price * $product->pivot->quantity)
                    ]);
                } else {
                    // else create and attach
                    $order = $this->user->placeOrder($product, $payment);
                }
            }
        });
        // empty cart
        $this->empty();
        // charge customer if payment is not cash
        if ($payment == 'wallet') {
            $orders->each->chargeCustomer();
        }
    }

    /**
     * Add product to cart.
     *
     * @param Product $product
     */
    public function addToCart(Product $product)
    {
        // if exists increment
        if ($this->products->contains($product)) {
            $quantity = $this->products()->find($product->id)->pivot->quantity + 1;
            $this->products()->updateExistingPivot($product, ['quantity' => $quantity]);
        } else {
            // else attach
            $this->products()->attach($product);
        }
    }

    /**
     * Reomve product from cart.
     *
     * @param Product $product
     */
    public function removeFromCart(Product $product)
    {
        $this->products()->detach($product);
    }
}
