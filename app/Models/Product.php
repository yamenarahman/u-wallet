<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];
    protected $appends = ['imagePath', 'path'];
    /**
     * Belongs to a category.
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Belongs to many orders.
     */
    public function orders()
    {
        return $this->belongsToMany(Order::class)->withPivot([
            'price',
            'quantity',
            'subtotal'
        ]);
    }

    /**
     * Get image path.
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getImagePathAttribute()
    {
        return url('/storage/' . $this->attributes['image']);
    }

    /**
     * Path.
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getPathAttribute()
    {
        return url("/products/{$this->id}");
    }
}
