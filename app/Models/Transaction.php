<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = [];

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * Deposit transaction.
     *
     * @param $attributes
     */
    public static function makeDeposit($attributes)
    {
        static::create($attributes);
    }

    /**
     * Payment transaction.
     *
     * @param $orderId
     */
    public static function makePayment($orderId)
    {
        $order = Order::findOrFail($orderId);

        static::create([
            'creator_id' => $order->customer_id,
            'customer_id' => $order->customer_id,
            'order_id' => $order->id,
            'amount' => -abs($order->total),
            'type' => 'payment'
        ]);
    }

    /**
     * Refund transaction.
     *
     * @param $orderId
     */
    public static function makeRefund($orderId)
    {
        $order = Order::findOrFail($orderId);

        static::create([
            'creator_id' => $order->customer_id,
            'customer_id' => $order->customer_id,
            'order_id' => $order->id,
            'amount' => $order->total,
            'type' => 'refund'
        ]);
    }
}
