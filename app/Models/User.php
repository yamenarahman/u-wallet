<?php

namespace App\Models;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $guarded = [];
    protected $appends = ['role'];
    protected $with = ['roles'];

    /**
     * Owns a store.
     */
    public function store()
    {
        if ($this->hasRole('store owner')) {
            return $this->hasOne(Store::class, 'owner_id');
        }
    }

    /**
     * Has many orders.
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'customer_id');
    }

    /**
     * Has one cart.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cart()
    {
        return $this->hasOne(Cart::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'customer_id');
    }

    public function profile()
    {
        if ($this->hasRole('customer')) {
            return $this->hasOne(Profile::class);
        }
    }

    /**
     * User's role name.
     *
     * @return mixed
     */
    public function getRoleAttribute()
    {
        return optional($this->roles->first())->name;
    }

    /**
     * User's wallet balance.
     *
     * @return mixed
     */
    public function getBalanceAttribute()
    {
        return $this->hasRole('customer')
            ? $this->transactions->sum('amount')
            : 0;
    }

    public function updatePassword($password)
    {
        $this->update(['password' => \Hash::make($password)]);
    }

    public function placeOrder($product, $payment)
    {
        $order = $this->orders()->create([
            'store_id' => $product->category->store_id,
            'payment' => $payment
        ]);

        // attach product to order
        return tap($order, function ($order) use ($product) {
            $order->products()->attach($product, [
                'price' => $product->price,
                'quantity' => $product->pivot->quantity,
                'subtotal' => ($product->price * $product->pivot->quantity)
            ]);
        });
    }

    public function hasSufficientBalance()
    {
        return $this->balance >= $this->cart->total;
    }
}
