<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];
    protected $appends = ['path'];
    protected $withCount = ['products'];

    /**
     * Belongs to a store.
     */
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    /**
     * Has many products.
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * Path.
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getPathAttribute()
    {
        return url("/categories/{$this->id}");
    }
}
