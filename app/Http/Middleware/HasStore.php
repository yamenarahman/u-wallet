<?php

namespace App\Http\Middleware;

use Closure;

class HasStore
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->hasRole('store owner') and !$request->user()->store()->exists()) {
            abort(403, 'You have no store, please contact site administrator.');
        }

        return $next($request);
    }
}
