<?php

namespace App\Http\Middleware;

use Closure;

class HasProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->hasRole('customer') and ! $request->user()->profile()->exists()) {
            return redirect('/settings')->with([
                'type' => 'warning',
                'message' => 'Please update your profile'
            ]);
        }

        return $next($request);
    }
}
