<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\Product;
use App\Models\Category;

class SearchController extends Controller
{
    public function index()
    {
        if (request()->has('query')) {
            $stores = Store::where('name', 'like', '%' . request('query') . '%')->get();
            $categories = Category::where('name', 'like', '%' . request('query') . '%')->get();
            $products = Product::where('title', 'like', '%' . request('query') . '%')->get();

            return view('search', compact('stores', 'categories', 'products'));
        }

        return back();
    }
}
