<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Store;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'has_store', 'has_profile']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        switch (auth()->user()->role) {
            case 'admin':
                return redirect('/admin/dashboard');
            case 'store owner':
                return redirect('/owner/dashboard');
        }

        $stores = Store::orderBy('name')->get();
        $products = Product::with('category.store')->latest()->limit(12)->get()->shuffle();

        return view('home', compact('stores', 'products'));
    }
}
