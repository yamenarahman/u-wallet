<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Notifications\YouHaveNewOrder;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $cart = auth()->user()->cart->load('products.category.store');
        $order = Order::first();

        // $order->store->owner->notify(new YouHaveNewOrder($order));
        return view('cart', compact('cart'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // add_item or remove_item
        if ($request->has('add_item')) {
            $request->validate(['product_id' => 'required|numeric|exists:products,id']);

            $product = Product::findOrFail($request->product_id);

            auth()->user()->cart->addToCart($product);

            return back()->with([
               'type' => 'success',
               'message' => 'Product is added to cart successfully!'
            ]);
        } elseif ($request->has('remove_item')) {
            $request->validate(['product_id' => 'required|numeric|exists:products,id']);

            $product = auth()->user()->cart->products()->findOrFail($request->product_id);

            auth()->user()->cart->removeFromCart($product);

            return back()->with([
                'type' => 'error',
                'message' => 'Product is removed from cart successfully!'
            ]);
        }
    }

    public function destroy()
    {
        auth()->user()->cart->empty();

        return back()->with([
            'type' => 'error',
            'message' => 'Cart is now empty!'
        ]);
    }
}
