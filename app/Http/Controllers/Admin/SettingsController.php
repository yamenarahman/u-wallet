<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function index()
    {
        return view('admin.settings');
    }

    public function store(Request $request)
    {
        $request->validate([
            'current_password' => 'required|string',
            'password' => 'required|string|min:6|confirmed'
        ]);

        if (! \Hash::check($request->current_password, auth()->user()->password)) {
            return back()->with([
                'type' => 'error',
                'message' => 'You have entered wrong password!'
            ]);
        }

        auth()->user()->updatePassword($request->password);

        return back()->with([
            'type' => 'success',
            'message' => 'Your password is updated successfully!'
        ]);
    }
}
