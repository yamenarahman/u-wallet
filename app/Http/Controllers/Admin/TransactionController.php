<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    public function index()
    {
        $transactions = Transaction::whereHas('customer')
            ->whereHas('creator')
            ->with(['creator', 'customer', 'order'])->latest()->get();

        $customers = User::role('customer')->get();

        return view('admin.transactions.index', compact('transactions', 'customers'));
    }

    public function store(Request $request)
    {
        $attributes = $request->validate([
            'customer_id' => 'required|numeric|exists:users,id',
            'amount' => 'required|numeric'
        ]);

        $attributes['creator_id'] = auth()->id();
        $attributes['type'] = 'deposit';

        Transaction::makeDeposit($attributes);

        return back()->with([
            'type' => 'success',
            'message' => 'Transaction is created successfully!'
        ]);
    }

    public function destroy(Transaction $transaction)
    {
        $this->authorize('delete', $transaction);

        $transaction->delete();

        return back()->with([
            'type' => 'error',
            'message' => 'Transaction is deleted successfully!'
        ]);
    }
}
