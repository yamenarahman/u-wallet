<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::with('owner')->orderBy('name')->get();
        $owners = User::role('store owner')->get();

        return view('admin.stores.index', compact('stores', 'owners'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->validate([
            'name' => 'required|string|unique:stores',
            'owner_id' => 'required|numeric|exists:users,id'
        ]);

        $owner = User::find($request->owner_id);

        if ($owner->store()->exists()) {
            return back()->with([
                'type' => 'error',
                'message' => 'Store owner already has a store!'
            ]);
        }

        Store::create($attributes);

        return back()->with([
            'type' => 'success',
            'message' => 'Store is created successfully!'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        $attributes = $request->validate([
            'name' => 'required|string|unique:stores,id,' . $store->id,
            'owner_id' => 'required|numeric|exists:users,id'
        ]);

        $owner = User::find($request->owner_id);

        if ($owner->store()->exists() and $owner->id != $store->owner_id) {
            return back()->with([
                'type' => 'error',
                'message' => 'Store owner already has a store!'
            ]);
        }

        $store->update($attributes);

        return back()->with([
            'type' => 'success',
            'message' => 'Store is updated successfully!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        $store->delete();

        return back()->with([
            'type' => 'error',
            'message' => 'Store is deleted successfully!'
        ]);
    }
}
