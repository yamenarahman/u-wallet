<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('name')->get();
        $roles = ['admin', 'store owner', 'customer'];

        return view('admin.users.index', compact('users', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'role' => ['required', Rule::in(['admin', 'store owner', 'customer'])]
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => \Hash::make('123456')
        ]);

        $user->assignRole($request->role);

        return back()->with([
            'type' => 'success',
            'message' => 'User is created successfully!'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique:users,id,' . $user->id,
            'role' => ['required', Rule::in(['admin', 'store owner', 'customer'])]
        ]);

        $user->update([
           'name' => $request->name,
           'email' => $request->email
        ]);

        if ($request->role != $user->role) {
            $user->syncRoles($request->role);
        }

        return back()->with([
            'type' => 'success',
            'message' => 'User is updated successfully!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return back()->with([
            'type' => 'error',
            'message' => 'User is deleted successfully!'
        ]);
    }
}
