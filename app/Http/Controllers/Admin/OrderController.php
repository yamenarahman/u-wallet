<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::whereHas('customer')
            ->whereHas('store')
            ->with(['products', 'store', 'customer'])->latest()->get();

        return view('admin.orders.index', compact('orders'));
    }
}
