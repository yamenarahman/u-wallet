<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $notifications = auth()->user()->unreadNotifications;

        return response()->json(compact('notifications'));
    }

    public function update(Request $request, $id)
    {
        $notification = auth()->user()->notifications()->findOrFail($id);

        $notification->markAsRead();

        return response()->json(['type' => 'success']);
    }

    public function destroy()
    {
        auth()->user()->unreadNotifications()->update(['read_at' => now()]);

        return response()->json([
           'type' => 'success',
            'message' => 'Notifications are cleared!'
        ]);
    }
}
