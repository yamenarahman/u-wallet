<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:customer']);
    }

    public function index()
    {
        return view('settings');
    }

    public function store(Request $request)
    {
        if ($request->has('update_password')) {
            $request->validate([
                'current_password' => 'required|string',
                'password' => 'required|string|min:6|confirmed'
            ]);

            if (!\Hash::check($request->current_password, auth()->user()->password)) {
                return back()->with([
                    'type' => 'error',
                    'message' => 'You have entered wrong password!'
                ]);
            }

            auth()->user()->updatePassword($request->password);

            return back()->with([
                'type' => 'success',
                'message' => 'Your password is updated successfully!'
            ]);
        } elseif ($request->has('update_profile')) {
            $attributes = $request->validate([
                'phone' => 'required|string',
                'avatar' => 'nullable|image|max:8000',
                'faculty' => 'nullable'
            ]);

            auth()->user()->profile()->updateOrCreate(['user_id' => auth()->id()], $attributes);

            return back()->with([
                'type' => 'success',
                'message' => 'Settings are updated successfully!'
            ]);
        }

        return back();
    }
}
