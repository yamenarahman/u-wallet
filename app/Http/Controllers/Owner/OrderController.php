<?php

namespace App\Http\Controllers\Owner;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = auth()->user()->store->orders()->with(['customer', 'products'])->latest()->paginate(25);

        return view('owner.orders.index', compact('orders'));
    }

    public function show(Order $order)
    {
        $this->authorize('view', $order);

        $order->load(['customer', 'products']);

        return view('owner.orders.show', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $this->authorize('update', $order);

        $request->validate([
            'status' => ['required', Rule::in(['rejected', 'in progress', 'ready', 'closed'])]
        ]);

        $order->update(['status' => $request->status]);

        return back()->with([
            'Order status updated successfully!'
        ]);
    }
}
