<?php

namespace App\Http\Controllers\Owner;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = auth()->user()->store->categories;

        return view('owner.categories.index', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->validate([
           'name' => 'required|string'
        ]);

        auth()->user()->store->categories()->create($attributes);

        return back()->with([
            'type' => 'success',
            'message' => 'Category is created successfully!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $this->authorize('view', $category);

        $category->load('products');

        return view('owner.categories.show', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->authorize('update', $category);

        $attributes = $request->validate([
            'name' => 'required|string|unique:categories,id,' . $category->id,
        ]);

        $category->update($attributes);

        return back()->with([
            'type' => 'success',
            'message' => 'Category is updated successfully!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $this->authorize('delete', $category);

        $category->delete();

        return back()->with([
            'type' => 'error',
            'message' => 'Category is deleted successfully!'
        ]);
    }
}
