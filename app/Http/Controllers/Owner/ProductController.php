<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = auth()->user()->store->products()->with('category')->get();
        $categories = auth()->user()->store->categories;

        return view('owner.products.index', compact('products', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->validate([
            'title' => 'required|string',
            'description' => 'nullable',
            'category_id' => 'required|exists:categories,id',
            'image' => 'nullable|image|max:2048',
            'price' => 'required|numeric'
        ]);

        $category = auth()->user()->store->categories()->findOrFail($request->category_id);

        if ($request->image) {
            $attributes['image'] = $request->file('image')->store('images', 'public');
        }

        $category->products()->create($attributes);

        return back()->with([
            'type' => 'success',
            'message' => 'Product is created successfully!'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->authorize('update', $product);

        $attributes = $request->validate([
            'title' => 'required|string',
            'description' => 'nullable',
            'category_id' => 'required|exists:categories,id',
            'image' => 'nullable|image|max:2048',
            'price' => 'required|numeric'
        ]);

        $category = auth()->user()->store->categories()->findOrFail($request->category_id);

        if ($request->image) {
            $attributes['image'] = $request->file('image')->store('images', 'public');
        }

        $product->update($attributes);

        return back()->with([
            'type' => 'success',
            'message' => 'Product is updated successfully!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->authorize('delete', $product);

        $product->delete();

        return back()->with([
            'type' => 'error',
            'message' => 'Product is deleted successfully!'
        ]);
    }
}
