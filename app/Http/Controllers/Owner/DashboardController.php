<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $stats = [
            'categories' => auth()->user()->store->categories_count ?? 0,
            'products' => auth()->user()->store->products_count ?? 0
        ];

        return view('owner.dashboard', compact('stats'));
    }
}
