<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function index()
    {
        return view('owner.settings');
    }

    public function store(Request $request)
    {
        if ($request->has('update_password')) {
            $request->validate([
                'current_password' => 'required|string',
                'password' => 'required|string|min:6|confirmed'
            ]);

            if (! \Hash::check($request->current_password, auth()->user()->password)) {
                return back()->with([
                    'type' => 'error',
                    'message' => 'You have entered wrong password!'
                ]);
            }

            auth()->user()->updatePassword($request->password);

            return back()->with([
                'type' => 'success',
                'message' => 'Your password is updated successfully!'
            ]);
        } elseif ($request->has('update_store')) {
            $request->validate(['store' => 'required|string']);

            auth()->user()->store->update(['name' => $request->store]);

            return back()->with([
                'type' => 'success',
                'message' => 'Your store name is updated successfully!'
            ]);
        }

        return back();
    }
}
