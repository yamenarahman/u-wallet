<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = auth()->user()->orders()->with(['store', 'products'])->latest()->paginate(25);

        return view('orders.index', compact('orders'));
    }

    public function show(Order $order)
    {
        $this->authorize('view', $order);

        $order->load(['store', 'products']);

        return view('orders.show', compact('order'));
    }

    public function store(Request $request)
    {
        $request->validate(['payment' => ['required', Rule::in(['cash', 'wallet'])]]);

        // check payment wallet option
        if ($request->payment == 'wallet') {
            // check sufficient balance
            if (!auth()->user()->hasSufficientBalance()) {
                return back()->with([
                    'type' => 'error',
                    'message' => 'You have insufficient balance!'
                ]);
            }
        }

        // cart checkout
        auth()->user()->cart->checkout($request->payment);

        return redirect('orders')->with([
            'type' => 'success',
            'message' => 'Your order is created successfully!'
        ]);
    }

    public function update(Request $request, Order $order)
    {
        $this->authorize('update', $order);

        if ($request->has('add_notes')) {
            $attributes = $request->validate(['notes' => 'required|string']);

            $order->update($attributes);
        } elseif ($request->has('add_schedule')) {
            $attributes = $request->validate([
                'pickup_at' => ['required', Rule::in(['asap', '30m', '1h', '2h', '3h'])]
            ]);

            switch ($attributes['pickup_at']){
                case 'asap':
                    $pickup = null;
                    break;
                case '30m':
                    $pickup = now()->addMinutes(30);
                    break;
                case '1h':
                    $pickup = now()->addHour();
                    break;
                case '2h':
                    $pickup = now()->addHours(2);
                    break;
                case '3h':
                    $pickup = now()->addHours(3);
                    break;
            }

            $order->update(['pickup_at' => $pickup]);
        }

        return back()->with([
            'type' => 'success',
            'message' => 'Your order is updated successfully!'
        ]);
    }
}
