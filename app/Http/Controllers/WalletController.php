<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;

class WalletController extends Controller
{
    public function index()
    {
        $transactions = auth()->user()->transactions()->with(['order', 'creator'])->get();

        return view('wallet', compact('transactions'));
    }
}
