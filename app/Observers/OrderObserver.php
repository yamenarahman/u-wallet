<?php

namespace App\Observers;

use App\Models\Order;
use App\Events\NewNotification;
use App\Models\Transaction;
use App\Notifications\YouHaveNewOrder;
use App\Notifications\YourOrderStatusIsUpdated;

class OrderObserver
{
    /**
     * Handle the order "created" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
        // notify store owner.
        $order->store->owner->notify(new YouHaveNewOrder($order));
        $message = 'You have new Order.';
        $action = url("/owner/orders/{$order->id}");

        // dispatch notification event.
        NewNotification::dispatch($order->store->owner, $message, $action);
    }

    /**
     * Handle the order "updated" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {
        // if status dirty
        if ($order->isDirty('status')){
            // notify customer.
            $order->customer->notify(new YourOrderStatusIsUpdated($order));
            $message = "Your order #{$order->id} is {$order->status}";
            $action = url("/orders/{$order->id}");

            // dispatch notification event.
            NewNotification::dispatch($order->customer, $message, $action);

            // refund customer if order is rejected.
            if ($order->status == 'rejected' and $order->payment == 'wallet'){
                Transaction::makeRefund($order->id);
            }
        }
    }
}
