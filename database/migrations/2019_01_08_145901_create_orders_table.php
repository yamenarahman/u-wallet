<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('store_id')->index();
            $table->unsignedInteger('customer_id')->index();
            $table->enum('payment', ['cash', 'wallet']);
            $table->enum('status', ['pending', 'rejected', 'in progress', 'ready', 'closed'])->default('pending');
            $table->text('notes')->nullable();
            $table->timestamp('pickup_at')->nullable();
            $table->foreign('store_id')->references('id')->on('stores');
            $table->foreign('customer_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
