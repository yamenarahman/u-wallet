<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // admin
        User::create([
            'name' => 'admin',
            'email' => 'admin@u-wallet.com',
            'password' => Hash::make('password'),
            'email_verified_at' => now()
        ])->assignRole('admin');

        // customer
        $customer = User::create([
            'name' => 'customer',
            'email' => 'customer@u-wallet.com',
            'password' => Hash::make('password'),
            'email_verified_at' => now()
        ])->assignRole('customer');

        // create cart
        $customer->cart()->create();

        // create profile

        // store owner
        User::create([
            'name' => 'store owner',
            'email' => 'storeowner@u-wallet.com',
            'password' => Hash::make('password'),
            'email_verified_at' => now()
        ])->assignRole('store owner');


        // All store owners from 3 -> 13
        factory('App\Models\User', 9)->create()->each->assignRole('store owner');
    }
}
