<?php

use Faker\Generator as Faker;

$autoIncrement = autoIncrement();

$factory->define(App\Models\Store::class, function (Faker $faker) use ($autoIncrement){
    $autoIncrement->next();

    return [
        'owner_id' => $autoIncrement->current(),
        'name' => 'Store ' . rand(1, 999)
    ];

});

function autoIncrement()
{
    for ($i = 2; $i <= 12; $i++) {
        yield $i;
    }
}
