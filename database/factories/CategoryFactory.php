<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Category::class, function (Faker $faker) {
    return [
        'store_id' => rand(1, 10),
        'name' => 'Category ' . rand(1, 99),
    ];
});
