<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Product::class, function (Faker $faker) {
    return [
        'category_id' => rand(1, 20),
        'title' => 'Product ' . rand(1, 50),
        'price' => $faker->randomFloat(2, 5, 100),
        'description' => implode(',', $faker->paragraphs(4)),
    ];
});
